# Copyright 2024 Bob Proulx <bob@proulx.com>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# This makefile is used only if you run GNU Make.
# This provides a convenience of automatically running configure
# if it has not previously been run.

# Systems where /bin/sh is not the default shell need this.  The $(shell)
# command below won't work with e.g. stock DOS/Windows shells.
SHELL = /bin/sh

have-Makefile := $(shell test -f Makefile && echo yes)
have-configure := $(shell test -f configure && echo yes)

# If the user runs GNU make but has not yet run ./configure,
# give them a diagnostic.
ifeq ($(have-Makefile),yes)

include Makefile

else

ifeq ($(have-configure),yes)

all:
	@echo There seems to be no Makefile in this directory.
	@echo "Running ./configure before running 'make'."
	sh ./configure
	@$(MAKE)

else

all:
	@echo There seems to be no Makefile in this directory.
	@echo There also does not seem to be a configure script yet.
	@echo "Running 'autoreconf --install' before running 'make'."
	autoreconf --install
	@$(MAKE)

endif

endif

have-Makefile-maint := $(shell test -f Makefile.maint && echo yes)
ifeq ($(have-Makefile-maint),yes)
include Makefile.maint
endif

# Tell version 3.79 and up of GNU make to not build goals in this
# directory in parallel.  This is necessary in case someone tries to
# build multiple targets on one command line.
.NOTPARALLEL:
