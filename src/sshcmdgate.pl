#!/usr/bin/env perl

# This program filters ssh requests.

# This program is to be configured via ForcedCommand to run instead of
# the configured login shell.  Having been invoked in the process flow
# this script then filters which commands are allowed to execute.

# Copyright 2024 Bob Proulx <bob@proulx.com> and Corwin Brust <corwin@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use File::Basename;
use Getopt::Long;

use strict;
use warnings FATAL => 'all';

# Ensure a standard behavior regardless of caller locale setting.
$ENV{'LC_ALL'} = "C";

my $progname = "sshcmdgate";
my $debug = 0;
my $verbose = 0;
my $version = '@VERSION@';
my $help = 0;
my $just_print = 0;
my $print_version = 0;
my $configfile = '/usr/local/etc/sshcmdgate.conf';
my $logfile;
my $chainshell;
my $chain;
my $logall = 0;
my $wrapall = 0;
my $wrapstr;
my @allowlist;

sub atexit {
    local($?);
}

sub END {
    &atexit();
}

sub sighandler {
    my $sig = shift;
    STDERR->print("$progname: signal $sig received, cleaning up.\n");
    &atexit();
    $SIG{$sig} = 'DEFAULT';
    kill($sig,$$);
}

sub print_short_usage {
    print $progname, ": Use --help to give you more information.\n";
}

sub print_help {
    print <<'EOF';
Usage: sshcmdgate [options]
Filter ssh requests via ForcedCommand.

     --help              print this help message
     --version           print program version
  -d,--debug             debug messages on
  -v,--verbose           verbose messages on
  -C,--config           config file

Examples:

  command="/usr/local/bin/sshcmdgate" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKsc4DmqTOZsCWsZPSh0GUuDU2eNQJ3fthkRDl0YS6Ur user1@host2
  command="/usr/local/bin/sshcmdgate --config /usr/local/etc/sshcmdgate.conf" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKsc4DmqTOZsCWsZPSh0GUuDU2eNQJ3fthkRDl0YS6Ur user1@host2

Or this in sshd_config.

    Match User root
    Match ALL
        ForceCommand /usr/local/sbin/sshcmdgate

Report bugs to <savannah-hackers-public@gnu.org>.
EOF
}

sub print_version {
    print "$progname $version\n";
    print <<'EOF';
Copyright (C) 2024 Bob Proulx.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Bob Proulx <bob@proulx.com>.
EOF
}

sub read_config {
    my $configfile = shift;
    open(my $fh,"<",$configfile)
        or die "Error: failed to open: $!\n";
    while (my $line = <$fh>) {
        chomp($line);
        next if $line =~ /^\s*#/; # skip full line comments
        $line =~ s/^\s+|\s+$//s;  # full trim, redundant
        next if length($line) == 0;
        # Examples of config lines:
        #     logfile /var/log/sshcmdgate/sshcmdgate.log
        #     logall
        #     wrapall
        #     wrapall -wrap
        #     chainshell /usr/bin/git-shell
        #     chainshell /bin/sh
        #     chain /usr/bin/cvs
        #     allow git-(receive|upload)-pack\s'/srv/git/\S+'
        #     allow scp\s+-t\s+/releases/\S
        my ($user,$value) = split(' ',$line,2); # limiting split enables pattern to have spaces

        if ($line =~ m/^allow\s/) {
            $line =~ s/^allow\s+//;
            push(@allowlist,$line);
        } elsif ($line =~ m/^chainshell\s/) {
            $line =~ s/^chainshell\s+//;
            $chainshell = $line;
        } elsif ($line =~ m/^chain\s/) {
            $line =~ s/^chain\s+//;
            $chain = $line;
        } elsif ($line =~ m/^logall\s/) {
            $logall = 1;
        } elsif ($line =~ m/^wrapall\s/) {
            $wrapall = 1;
            $line =~ s/^wrapall\s+//;
            $wrapstr = $line;
        } elsif ($line =~ m/^logfile\s/) {
            $line =~ s/^logfile\s+//;
            $logfile = $line;
        } else {
            die "Error: invalid line $fh->$.\n";
        }
    }
    close($fh)
        or die "Error: failed to close: $configfile\n";
    if (!$logfile && $logall) {
        die "Error: logall configured but missing logfile\n";
    }
}

sub process_options {
    $Getopt::Long::bundling = 1;
    $Getopt::Long::order = $REQUIRE_ORDER;

    my %longopts = (
        "C=s"            => \$configfile,
        "config=s"       => \$configfile,
        "d"              => \$debug,
        "debug"          => \$debug,
        "help"           => \$help,
        "v"              => \$verbose,
        "verbose"        => \$verbose,
        "version"        => \$print_version,
        );

    if (!&GetOptions(%longopts)) {
        warn $progname, ": invalid command line option\n";
        &print_short_usage();
        return undef;
    }

    if ($help) {
        &print_help();
        exit(0);
    }

    if ($print_version) {
        &print_version();
        exit(0);
    }

    if (defined($configfile)) {
        if (&read_config($configfile)) {
            die "Error: failed to read config file: \"$configfile\"\n";
        }
    }
    return 1;
}

sub main {
    $SIG{'HUP'} = \&sighandler;
    $SIG{'INT'} = \&sighandler;
    $SIG{'QUIT'} = \&sighandler;
    $SIG{'TERM'} = \&sighandler;
    if (!&process_options()) {
        die $progname, ": exiting\n";
    }
    if (!exists($ENV{'SSH_ORIGINAL_COMMAND'})) {
        printf("\n");
        printf("Hello $ENV{'USER'}!\n");
        printf("You have successfully authenticated but");
        printf(" interactive shell access is not allowed.\n");
        printf("\n");
        return 0;
    }
    my $cmd = $ENV{'SSH_ORIGINAL_COMMAND'};
    undef $ENV{'SSH_ORIGINAL_COMMAND'};
    my $allow_cmd = 0;
    foreach my $allowpat (@allowlist) {
        if ($cmd =~ m/^$allowpat$/) {
            $allow_cmd = 1;
            last;
        }
    }
    my $prog;
    my @cmdarr;
    # A chainshell is a shell that accepts -c command options.  This
    # will either be a full shell like /bin/sh or it will be a
    # firewall shell itself like the /usr/bin/git-shell utility.
    # Examples of possible chainshell configurations.
    #     chainshell /usr/bin/git-shell
    #     chainshell /bin/sh
    if (defined($chainshell)) {
        # When we chain to a shell we are passing responsibility for
        # the firewall to that shell.  This allows us to log it and
        # learn what needs to be firewalled before firewalling.
        $allow_cmd = 1;
        @cmdarr = ( basename($chainshell), "-c", $cmd );
        $prog = $chainshell;
    } else {
        # Not using a chain shell.  We execute the chain program directly.  (Without "-c").
        # Example of possible chain program.
        #     chain /usr/bin/cvs
        @cmdarr = split(' ',$cmd);
        if (defined($chain)) {
            $prog = $chain;
        } else {
            $prog = $cmdarr[0];
            if ($wrapall) {
                $prog .= $wrapstr;
            }
        }
    }
    # Not using a chain program.  We execute the firewall gated program directly.
    if ($logall) {
        open(my $fh,">>",$logfile)
            or die "Error: failed to open for mode >>: $logfile: $!\n";
        $fh->printf("$allow_cmd $cmd|%s:%s:%s\n",$prog,$wrapstr,join(',',@cmdarr));
        close($fh)
            or die "Error: failed to close logfile: $!\n";
    }
    if (!$allow_cmd && !defined($chainshell)) {
        die "Error: command unavailable: $cmd\n";
    }
    exec { $prog } @cmdarr
        or die "Error: Unable to execute command: $cmd\n";
}

if (__FILE__ eq $0) {
    exit(&main(@ARGV));
}

1;
