#!/bin/sh

# This program implements a shell command firewall.

# This program is to be configured to run as the user's login shell.
# Having been invoked in the process flow this program then filters
# which commands are allowed to execute.  The design vision is that
# this will work in conjunction with an ssh ForceCommand program and
# the two of these programs together will implement a generic command
# firewall policy layer.

# Copyright 2024 Bob Proulx <bob@proulx.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Ensure a standard behavior regardless of caller locale setting.
export LC_ALL=C

progname=shellcmdfw
version=@VERSION@

print_help() {
    # This script is designed to use help2man to build the man page
    # directly from the script itself.
    #   help2man shellcmdfw
    # Check the man page formatting after making changes here.
    cat <<'EOF'
Implement a shell command firewall.
Usage: shellcmdfw [options]
Options:
  -C,--config        config file
  -c, --cmd=STRING   sh -c compatible command option
  -n, --dry-run      not really, just print what would be done
  -q, --quiet        quiet operation
  -v, --verbose      verbose operation
      --help         print this help message
      --version      print program version

Examples:

  user1:x:1000:1000::/var/local/shellcmdfw:/usr/local/bin/shellcmdfw

Report bugs to <savannah-hackers-public@gnu.org>.
EOF
}

print_version() {
    cat <<EOF
$progname $version
Copyright (C) 2024 Bob Proulx.
Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.

Written by Bob Proulx <bob@proulx.com>.
EOF
}

SHORTOPTS="c:nqv"
LONGOPTS="cmd:,dry-run,help,quiet,verbose,version"

if $(getopt -T >/dev/null 2>&1) ; [ $? = 4 ] ; then # New longopts getopt.
    OPTS=$(getopt -o $SHORTOPTS --long $LONGOPTS -n "$progname" -- "$@")
else # Old classic getopt.
    # Special handling for --help and --version on old getopt.
    case $1 in --help) print_help ; exit 0 ;; esac
    case $1 in --version) print_version ; exit 0 ;; esac
    OPTS=$(getopt $SHORTOPTS "$@")
fi

if [ $? -ne 0 ]; then
    echo "'$progname --help' for more information" 1>&2
    exit 1
fi

eval set -- "$OPTS"

cmd=""
configfile="/usr/local/etc/shellcmdfw.conf"
notreally=false
quiet=false
verbose=false
while [ $# -gt 0 ]; do
    : debug: "$1"
    case $1 in
	-C|--config)
	    configfile=$2
	    shift 2
	    ;;
	-c|--cmd)
	    cmd=$2
	    shift 2
	    ;;
	--help)
	    print_help
	    exit 0
	    ;;
	-n|--dry-run)
	    notreally=true
	    shift
	    ;;
	--quiet)
	    quiet=true
	    shift
	    ;;
	-v|--verbose)
	    verbose=true
	    shift
	    ;;
	--version)
	    print_version
	    exit 0
	    ;;
	--)
	    shift
	    break
	    ;;
	*)
	    echo "Internal Error: option processing error: $1" 1>&2
	    exit 1
	    ;;
    esac
done

if [ -z "$cmd" ]; then
    cat <<EOF

Hello $USER!
You have successfully authenticated but interactive login access is not allowed.

EOF
    exit 0
fi

# Read the config file.  Match command against each allow line as it is read.
command=""
while IFS= read -r line; do
    case $line in
	allow*)
	    allowed=${line#allow }	# remove suffix "allow " leaving remainder
	    if [ "$cmd" = "$allowed" ]; then
		# Is a match.  Break out of loop to invoke the command.
		command=$allowed
		break
	    fi
	    ;;
    esac
done <"$configfile"

# If we matched with a command then exec it.
if [ -n "$command" ]; then
    if $notreally; then
	# logger -t shellcmdfw "dry run: $SSH_ORIGINAL_COMMAND"
	echo exec "$command"
	exit 0
    fi
    # logger -t shellcmdfw "$command, $SSH_ORIGINAL_COMMAND"
    exec "$command"
    echo "Error: Failed exec: $command" 1>&2
    exit 1
fi

if [ $? -ne 0 ]; then
    echo "Error: failed to open config file: $configfile" 1>&2
    exit 1
fi

printf "Error: Command not understood: %s\n" "$cmd" 1>&2

exit 0
